# Guide d'apprentissage du developpmeent web

Ce projet contient un guide d'apprentissage de la programmation web basé sur le contenu de plusieurs formations, tutoriels, cours et pdf disponible sur le net. Pour faciliter la lecture, nous proposons dans ce guide l'apprentissage en se basant sur les [ feuilles de route](https://roadmap.sh/) de developpement front end et back-end. 


## Fichier de base 

* [Guide d'apprentissage du developpmeent web ](Developpement Web/guide d'apprentisage développement Web.pdf)

## Ressources
Ce guide contient egalement un ensemble des fichiers pdf pour permettre à l'apprenant d'avoir une idée sur differentes notions du front-end et du back-end.

Le guide est subdivisé en deux parties : 

## Front-end
<img src="Developpement Web/image/img_frontend.jpg">

## Back-end
<img src="Developpement Web/image/img_backend.jpg">

-----
Signalons que ce projet est encours de developpement.